from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'core/home.html')

def login(request):
    return render(request, 'core/login.html')

def donaciones(request):
    return render(request, 'core/donaciones.html')