from django.urls import path
from .views import home, login, donaciones

urlpatterns = [
    path('', home, name="home"),
    path('login/', login, name="login"),
    path('donaciones/', donaciones, name="donaciones")
]